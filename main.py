import komm
import numpy as np
from PIL import Image

awgn = komm.AWGNChannel(snr=10, signal_power=1)
psk = komm.PSKModulation(4, phase_offset=np.pi/4)

image = Image.open("duck.jpg")

# convert to grayscale
#image = image.convert('L')

shape = np.array(image).shape

print(f'Shape: {shape}')

# flatten array
imArray = np.array(image).flatten()

# convert and unpack uint8 pixel values into a binary sequence
numpyUnpacked = np.unpackbits(imArray)

print(numpyUnpacked)
print(f'Constellation layout (deg): {np.angle(psk.constellation)*180/np.pi}')

# modulate via QPSK
modulated = psk.modulate(numpyUnpacked)

# send thru AWGN channel
afterChannel = awgn(modulated)

# demodulate
demodulated = psk.demodulate(afterChannel)

# pack bitstream back to uint8
packedBits = np.packbits(demodulated)

# shape flattened uint8 array into original dimension
shaped = packedBits.reshape(shape)

transferedImage = Image.fromarray(shaped)
transferedImage.show()